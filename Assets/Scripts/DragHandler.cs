using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IDragHandler,IEndDragHandler,IBeginDragHandler
{
    [SerializeField] private GameObject item;
    [SerializeField] private GameObject item2;

    public static GameObject itemDragging;
    

    Vector3 startPosition;
     Transform startParent;
     Transform dragParent;

   
    void Start()
    {
        dragParent = GameObject.FindGameObjectWithTag("DragParent").transform;
    }
    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
        item.SetActive(false);
        item2.SetActive(true);
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        itemDragging = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;

        transform.SetParent(dragParent);

    }

    

    public void OnEndDrag(PointerEventData eventData)
    {
        itemDragging = null;
        if(transform.parent == dragParent)
        {
            transform.position = startPosition;
            transform.SetParent(startParent);
        }

        item.SetActive(true);
        item2.SetActive(false);
    }

    

    // Update is called once per frame
    void Update()
    {
        
    }
}
