using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class Ordenar : MonoBehaviour
{
    [SerializeField] private GameObject menu;
    [SerializeField] private GameObject aviso;
    [SerializeField] private GameObject detalles;

    [SerializeField] private GameObject botonOrdenar;
    [SerializeField] private GameObject botonDetalles;

    [SerializeField] private GameObject slot1;
    [SerializeField] private GameObject slot2;
    [SerializeField] private GameObject slot3;
    [SerializeField] private GameObject slot4;
    [SerializeField] private GameObject slot5;
    [SerializeField] private GameObject slot6;
    [SerializeField] private GameObject slot7;
    [SerializeField] private GameObject slot8;


    [SerializeField] private GameObject slotO1;
    [SerializeField] private GameObject slotO2;
    [SerializeField] private GameObject slotO3;
    [SerializeField] private GameObject slotO4;

    [SerializeField] private GameObject equipo;

    [SerializeField] private TextMeshProUGUI nombre;
    [SerializeField] private TextMeshProUGUI tipo;
    [SerializeField] private TextMeshProUGUI descrripcion;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (equipo.transform.childCount > 0)
        {
            //en caso de que tenga algo 
            
            nombre.text= equipo.transform.GetChild(0).GetChild(0).name;
            tipo.text = equipo.transform.GetChild(0).GetChild(1).name;
            descrripcion.text = equipo.transform.GetChild(0).GetChild(2).name;
            if (!menu.activeSelf)//el menu esta desactivado
            {
                botonDetalles.SetActive(true);
            }
            
        }
        else
        {
            detalles.SetActive(false);
            botonDetalles.SetActive(false);
        }
    }
    public void MenuOrdenarAbrir()
    {
        menu.SetActive(true);
        botonOrdenar.SetActive(false);
        botonDetalles.SetActive(false);
    }
    public void MenuOrdenarCerrar()
    {
        if (slotO1.transform.childCount == 0 && slotO2.transform.childCount == 0 &&
            slotO3.transform.childCount == 0 && slotO4.transform.childCount == 0)
        {
            menu.SetActive(false);
            aviso.SetActive(false);
            botonOrdenar.SetActive(true);
            botonDetalles.SetActive(true);
        }
        else
        {
            aviso.SetActive(true);
            
        }
        
        
    }
    public void MenuDetallesAbrir()
    {
        detalles.SetActive(true);
        botonOrdenar.SetActive(false);
        botonDetalles.SetActive(false);


    }
    public void MenuDetallesCerrar()
    {
        detalles.SetActive(false);
        botonOrdenar.SetActive(true);
        botonDetalles.SetActive(true);

    }
}
