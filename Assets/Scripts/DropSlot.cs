using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class DropSlot : MonoBehaviour,IDropHandler
{
    public GameObject item;
    private Image myImage;
    [SerializeField]private GameObject predeterminado;

    void Start()
    {
        myImage = GetComponent<Image>();
        if(predeterminado != null)
        {
            item = predeterminado;
            item.transform.SetParent(transform);
            item.transform.position = transform.position;
            myImage.color = new Color(1, 1, 1, 0.5411765f);
        }
    }
    public void OnDrop(PointerEventData eventData)
    {
        if(!item&& gameObject.transform.childCount == 0)
        {
            item = DragHandler.itemDragging;
            item.transform.SetParent(transform);
            item.transform.position = transform.position;
            myImage.color= new Color(1, 1, 1, 0.5411765f);
            
        }
    }


    void Update()
    {
        if (gameObject.transform.childCount > 0 )
        {
            
            myImage.color = new Color(1, 1, 1, 0.5411765f);
        }
        else
        {
            myImage.color = new Color(0.1886f, 0.1886f, 0.1886f, 0.5411765f);
        }
        if (item!= null&& item.transform.parent != transform)
        {
            item = null;
            myImage.color =new Color(0.1886f, 0.1886f, 0.1886f, 0.5411765f);
        }
        
    }

}
